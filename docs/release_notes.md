# Release notes

## 1.0.3

Fixes:

* Fix crashing with 2d slices are taken from larger set of dicoms (as in volumetric
image) and included on their own.

Notes: Categories used in release notes should match one of the following:

* Fixes
* Enhancements
* Documentation
* Maintenance
