"""_summary_."""
from dataclasses import dataclass

import numpy as np


@dataclass
class RoiLabel:
    """_summary_."""

    label: str
    index: int
    color: str
    RGB: list
    num_voxels: int = 0
    volume: float = 0.0

    def calc_volume(self, affine):
        """_summary_.

        Args:
            affine (_type_): _description_
        """
        self.volume = self.num_voxels * np.abs(np.linalg.det(affine[:3, :3]))
