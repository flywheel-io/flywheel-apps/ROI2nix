"""Preppers - Process 1 of 4.

These are designed to prepare any local data for roi creation.  

This typically means the following:
    1. copy the original data (nifti or dicom) to a "working" directory.
    (/flywheel/v0/work/original_image)
    2. create a second copy of the original data into an "roi" directory
    (/flywheel/v0/work/roi_image)

    For dicoms, this means either unzipping a zipped file or copying in a full
    directory.  For niftis this will mean just copying the file.

    The "roi" image will later be modified directly to create the ROI's
 
Responsibilities:
1. prepare an "original" working directory with a copy of the data
2. prepare an "roi" working directory with a copy of the data

Full process:
1. Prep
2. Collect
3. Create
4. Convert

"""
import logging
import os
import shutil
import zipfile
from abc import ABC, abstractmethod
from pathlib import Path

from fw_file.dicom import DICOMCollection
from fw_file.dicom.utils import sniff_dcm

from fw_gear_roi2nix.roi_tools import InvalidDICOMFile

log = logging.getLogger(__name__)


class BasePrepper(ABC):
    """_summary_.

    Raises:
        NotImplementedError: _description_

    """

    # Type key set on each base class to identify which class to instantiate
    type_ = None

    def __init__(self, work_dir, input_file_path):
        """_summary_.

        Args:
            work_dir (_type_): _description_
            input_file_path (_type_): _description_
        """
        if work_dir is None:
            work_dir = ""
        self.work_dir = Path(work_dir)
        self.output_dir = self.work_dir / "roi_image"
        self.orig_dir = self.work_dir / "original_image"
        self.input_file_path = input_file_path

    @abstractmethod
    def prep(self):
        """_summary_."""

    @classmethod
    def factory(cls, type_: str, work_dir, input_file_path):
        """Return an instantiated prepper."""
        for sub in cls.__subclasses__():
            if type_.lower() == sub.type_:
                return sub(work_dir, input_file_path)
        raise NotImplementedError(f"File type {type_} no supported")


class PrepDicom(BasePrepper):
    """_summary_.

    Raises:
        InvalidDICOMFile: _description_
    """

    type_ = "dicom"

    def prep(self):
        """Prepper for dicoms.

        For the dicom prepper, this does the following:
        1. if the dicom is zipped, unzip to `workdir/original_image`
         - if unzipped, copy the directory
        2. make a copy of the files in `workdir/roi_image` to be processed
        Returns:
        """
        self.move_dicoms_to_workdir()
        self.copy_dicoms_for_export()

    def move_dicoms_to_workdir(self):
        """_summary_."""
        # if archived, unzip dicom into work/dicom/

        if zipfile.is_zipfile(self.input_file_path):
            self.orig_dir.mkdir(parents=True, exist_ok=True)
            dcms = DICOMCollection.from_zip(self.input_file_path)
            dcms.to_dir(self.orig_dir)

        else:
            if not sniff_dcm(self.input_file_path):  # single-dicom
                log.warning(
                    "Input is missing file signature, "
                    "attempting to read as a single dicom "
                )

            if not os.path.exists(self.orig_dir):
                os.mkdir(self.orig_dir)

            shutil.move(self.input_file_path.as_posix(), self.orig_dir.as_posix())

    def copy_dicoms_for_export(self):
        """_summary_.

        Raises:
            InvalidDICOMFile: _description_
        """
        try:
            # Copy dicoms to a new directory
            if not os.path.exists(self.output_dir):
                shutil.copytree(self.orig_dir, self.output_dir)
        except Exception as e:
            log.exception(e)
            error_message = "Unable to find or open dicom file"
            raise InvalidDICOMFile(error_message)


class PrepNifti(BasePrepper):
    """_summary_."""

    type_ = "nifti-notimplemented"

    def prep(self):
        """_summary_."""
        pass
