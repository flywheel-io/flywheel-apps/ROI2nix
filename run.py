#!/usr/bin python3
"""Runfile for ROI2nix."""
import logging

from flywheel_gear_toolkit import GearToolkitContext

import fw_gear_roi2nix.ExportRois as ExportRois
from fw_gear_roi2nix.parser import parser

log = logging.getLogger(__name__)
print("^^^^ FILE")


def main(
    fw_client,
    save_combined_output,
    save_binary_masks,
    conversion_method,
    input_file_path,
    input_file_object,
    work_dir,
    output_dir,
    destination_type,
    save_slicer_color_table,
):
    """Execute the ROI2nix toolkit.

    Args:
        fw_client (_type_): _description_
        save_combined_output (_type_): _description_
        save_binary_masks (_type_): _description_
        conversion_method (_type_): _description_
        input_file_path (_type_): _description_
        input_file_object (_type_): _description_
        work_dir (_type_): _description_
        output_dir (_type_): _description_
        destination_type (_type_): _description_
        save_slicer_color_table (_type_): _description_
    """
    # Need updated file information.
    file_obj = input_file_object["object"]
    file_obj = fw_client.get_file(file_obj["file_id"])

    # The inv_reduced_aff may need to be adjusted for dicoms
    ExportRois.main(
        fw_client,
        file_obj,
        save_combined_output,
        save_binary_masks,
        conversion_method,
        input_file_path,
        input_file_object,
        work_dir,
        output_dir,
        destination_type,
        save_slicer_color_table,
    )


if __name__ == "__main__":

    with GearToolkitContext() as gear_context:
        gear_context.init_logging()
        (
            fw_client,
            save_combined_output,
            save_binary_masks,
            conversion_method,
            input_file_path,
            input_file_object,
            work_dir,
            output_dir,
            destination_type,
            save_slicer_color_table,
        ) = parser(gear_context)

        main(
            fw_client,
            save_combined_output,
            save_binary_masks,
            conversion_method,
            input_file_path,
            input_file_object,
            work_dir,
            output_dir,
            destination_type,
            save_slicer_color_table,
        )
